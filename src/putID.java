import org.apache.commons.io.FileUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class putID {
    static String filePath;
    static ArrayList<String> productsList;
    static Map<String, String> ManufacturerMap = new HashMap<>();


    static String[] extensions = new String[]{"csv"};


    static File htmlPath = new File("./Try");
    static Collection<File> allCSVFiles = FileUtils.listFiles(htmlPath, extensions, true);

    public putID() {
    }


    public static void main(String[] args) throws IOException {

        // readMan();
        for (File file : allCSVFiles) {
            productsList = new ArrayList<>();
            filePath = file.getPath();
            readEdit();
            save();

        }

    }

    /**
     * Delete first column from csv Files.
     * Can edit Parts[x], x = number of column that you need to delete
     *
     *
     * @throws IOException
     */

    public static void readEdit() throws IOException {
        File file = new File(filePath);
        BufferedReader br = new BufferedReader(new FileReader(file));
        String st;


        while ((st = br.readLine()) != null) {
            if (st.length() > 0) {
                String[] parts = st.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");

                if (parts.length > 0) {
                    int size = parts[0].length();
                    st = st.substring(size + 1);

                    // String test = ManufacturerMap.get(parts[0]);
                    // st = test + st;

                    productsList.add(st);
                }
            }

        }
    }


    /**
     * Read All DigiKey manufacturers from ManufacturerTable.csv and
     * put them into a <Manufacturer>HashMap
     *
     *
     * @throws IOException
     */
    public static void readMan() throws IOException {
        File file = new File("ManufacturerTable.csv");
        BufferedReader br = new BufferedReader(new FileReader(file));
        String st = br.readLine();

        while ((st = br.readLine()) != null) {
            if (st.length() > 0) {
                String[] parts = st.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
                if (parts.length > 0) {
                    ManufacturerMap.put(parts[1], parts[0]);
                }
            }
        }
    }

    public static void save() throws IOException {


        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath), "ISO-8859-1"));

        for (String test : productsList) {
            writer.write(test);
            writer.write("\n");
            writer.flush();
        }
        writer.close();
        System.out.println(filePath);
        System.out.println(productsList.size());

    }

}
